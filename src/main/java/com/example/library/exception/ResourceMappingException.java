package com.example.library.exception;

public class ResourceMappingException extends RuntimeException {

    public ResourceMappingException(final String message) {
        super(message);
    }

}
