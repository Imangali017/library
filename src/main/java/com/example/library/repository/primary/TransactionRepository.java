package com.example.library.repository.primary;

import com.example.library.models.primary.Transaction;
import com.example.library.models.primary.enums.TransactionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {
    List<Transaction> findByStatus(TransactionStatus returned);

    List<Transaction> findTransactionsByEndDateBefore(LocalDateTime dueDateThreshold);
}
