package com.example.library.rabbit;

import com.example.library.models.primary.dto.NotificationMessage;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class NotificationMessageHandler {

    private final JavaMailSender javaMailSender;

    @RabbitListener(queues = "#{rabbitMQConfig.getQueue()}")
    public void handleNotification(NotificationMessage notificationMessage) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");
            helper.setText(notificationMessage.getMessage(), true);
            helper.setTo(notificationMessage.getRecipientEmail());
            helper.setSubject(notificationMessage.getSubject());
            helper.setFrom("user@gmail.com");
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            log.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }

}


