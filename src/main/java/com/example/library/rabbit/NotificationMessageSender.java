package com.example.library.rabbit;

import com.example.library.models.primary.dto.NotificationMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NotificationMessageSender {

    private final RabbitTemplate rabbitTemplate;

    @Value("${app.rabbitmq.exchange}")
    private String exchange;

    @Value("${app.rabbitmq.routingKey}")
    private String routingKey;

    public void sendNotification(NotificationMessage notificationMessage) {
        rabbitTemplate.convertAndSend(exchange, routingKey, notificationMessage);
    }
}
