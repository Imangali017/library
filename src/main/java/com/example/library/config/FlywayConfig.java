package com.example.library.config;

import jakarta.annotation.PostConstruct;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class FlywayConfig {
    private DataSource primaryDataSource;
    private DataSource secondaryDataSource;
    @Value("${app.datasource.primary.flyway.locations}")
    private String flywayPrimaryLocations;

    @Value("${app.datasource.secondary.flyway.locations}")
    private String flywaySecondaryLocations;

    public FlywayConfig(@Qualifier("primaryDataSource") DataSource primaryDataSource,
                        @Qualifier("secondaryDataSource") DataSource secondaryDataSource) {
        this.primaryDataSource = primaryDataSource;
        this.secondaryDataSource = secondaryDataSource;
    }
  
    @PostConstruct
    public Flyway primaryFlyway() {
        Flyway flyway = Flyway.configure()
                .dataSource(primaryDataSource)
                .locations(flywayPrimaryLocations)
                .load();
        flyway.migrate();
        return flyway;
    }

    @PostConstruct
    public Flyway secondaryFlyway() {
        Flyway flyway = Flyway.configure()
                .dataSource(secondaryDataSource)
                .locations(flywaySecondaryLocations)
                .load();
        flyway.migrate();
        return flyway;
    }
}
