package com.example.library.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${app.rabbitmq.exchange}")
    private String exchange;

    @Value("${app.rabbitmq.queue}")
    private String queue;

    public String getQueue() {
        return queue;
    }

    @Bean
    public FanoutExchange notificationExchange() {
        return new FanoutExchange(exchange);
    }

    @Bean
    public Queue notificationQueue() {
        return new Queue(queue);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(notificationQueue()).to(notificationExchange());
    }
}

