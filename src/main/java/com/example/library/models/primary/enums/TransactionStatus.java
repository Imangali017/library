package com.example.library.models.primary.enums;

public enum TransactionStatus {
    ISSUED, // Выдано
    RETURNED, // Возвращено
}
