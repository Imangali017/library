package com.example.library.models.primary.dto;

import lombok.Data;

@Data
public class BookDto {

    private String title;
    private String author;
    private String category;
    private int year;
    private String description;
    private int views;
    private String coverUrl;
}
