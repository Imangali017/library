package com.example.library.models.primary.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BookSearchDto {
    private String author;
    private String title;
    private String category;
    private LocalDate readStartDate;
    private LocalDate readEndDate;
}
