package com.example.library.service;

import com.example.library.models.primary.Book;
import com.example.library.models.primary.dto.BookSearchDto;

import java.util.List;

public interface BookService {

    Book getBookById(Long id);
    List<Book> getAllBooks();
    Book createBook(Book book);
    void deleteBook(Long id);
    List<Book> searchBooksByCriteria(BookSearchDto searchDto);

}
