package com.example.library.service.impl;

import com.example.library.models.primary.Book;
import com.example.library.models.primary.dto.BookSearchDto;
import com.example.library.repository.primary.BookRepository;
import com.example.library.service.BookService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public Book getBookById(Long id) {
        return bookRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public List<Book> searchBooksByCriteria(BookSearchDto searchDto) {
        String author = searchDto.getAuthor();
        String title = searchDto.getTitle();
        String category = searchDto.getCategory();
        LocalDate readStartDate = searchDto.getReadStartDate();
        LocalDate readEndDate = searchDto.getReadEndDate();

        Specification<Book> specification = Specification.where(null);

        if (author != null && !author.isEmpty()) {
            specification = specification.and((root, query, builder) ->
                    builder.like(root.get("author"), "%" + author + "%"));
        }

        if (title != null && !title.isEmpty()) {
            specification = specification.and((root, query, builder) ->
                    builder.like(root.get("title"), "%" + title + "%"));
        }

        if (category != null && !category.isEmpty()) {
            specification = specification.and((root, query, builder) ->
                    builder.like(root.get("category"), "%" + category + "%"));
        }

        if (readStartDate != null && readEndDate != null) {
            specification = specification.and((root, query, builder) ->
                    builder.between(root.get("readStartDate"), readStartDate, readEndDate));
        }

        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "views"));
        Page<Book> resultPage = bookRepository.findAll(specification, pageable);

        return resultPage.getContent();
    }


}
