package com.example.library.service;

import com.example.library.models.primary.Transaction;
import com.example.library.models.primary.dto.NotificationMessage;
import com.example.library.rabbit.NotificationMessageSender;
import com.example.library.repository.primary.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class NotificationScheduler {

    private final NotificationMessageSender notificationMessageSender;
    private final TransactionRepository transactionRepository;

    @Scheduled(cron = "${spring.scheduled.notification-cron}")
    public void collectDataAndSendToRabbitMQ() {
        List<Transaction> approachingDueDateTransactions = findTransactionsWithApproachingDueDate(7);
        List<NotificationMessage> notificationMessages = createNotificationMessages(approachingDueDateTransactions);
        sendNotificationsToRabbitMQ(notificationMessages);
    }

    private List<NotificationMessage> createNotificationMessages(List<Transaction> approachingDueDateTransactions) {
        List<NotificationMessage> notificationMessages = new ArrayList<>();
        for (Transaction transaction : approachingDueDateTransactions) {
            NotificationMessage notificationMessage = new NotificationMessage();
            notificationMessage.setRecipientEmail(transaction.getUser().getEmail());
            notificationMessage.setSubject("Напоминание о сроке сдачи книги");
            notificationMessage.setMessage("Уважаемый " + transaction.getUser().getName() +
                    ", ваша книга '" + transaction.getBook().getTitle() + "' должна быть сдана к " +
                    transaction.getEndDate() + ". Просим вас вернуть книгу в библиотеку вовремя.");
            notificationMessages.add(notificationMessage);
        }
        return notificationMessages;
    }

    public List<Transaction> findTransactionsWithApproachingDueDate(int daysUntilDue) {
        LocalDateTime currentDate = LocalDateTime.now();
        LocalDateTime dueDateThreshold = currentDate.plusDays(daysUntilDue);
        return transactionRepository.findTransactionsByEndDateBefore(dueDateThreshold);
    }

    public void sendNotificationsToRabbitMQ(List<NotificationMessage> notificationMessages) {
        for (NotificationMessage message : notificationMessages) {
            notificationMessageSender.sendNotification(message);
        }
    }
}
