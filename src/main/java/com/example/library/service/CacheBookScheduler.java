package com.example.library.service;

import com.example.library.models.primary.Book;
import com.example.library.repository.primary.BookRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CacheBookScheduler {

    private final BookRepository bookRepository;
    private final CacheManager cacheManager;

    @PostConstruct
    @Scheduled(cron = "${spring.scheduled.cache-cron}")
    public void cacheTop10BooksByViews() {
        Pageable pageable = PageRequest.of(0, 10);
        Page<Book> top10Books = bookRepository.findTop10ByOrderByViewsDesc(pageable);
        cacheManager.getCache("top10BooksCache").put("top10", top10Books);
    }
                
    public Page<Book> getTop10BooksByViews(Pageable pageable) {
        return bookRepository.findTop10ByOrderByViewsDesc(pageable);
    }

    public List<Book> getTop10BooksByViewsFromService() {
        Pageable pageable = PageRequest.of(0, 10);
        Page<Book> top10Books = getTop10BooksByViews(pageable);
        return top10Books.getContent();
    }
}
