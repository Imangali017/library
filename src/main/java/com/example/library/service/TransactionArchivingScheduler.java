package com.example.library.service;

import com.example.library.models.primary.Transaction;
import com.example.library.models.primary.enums.TransactionStatus;
import com.example.library.models.secondary.Transactions;
import com.example.library.repository.primary.TransactionRepository;
import com.example.library.repository.secondary.TransactionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.scheduling.annotation.Scheduled;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionArchivingScheduler {

    private final TransactionRepository transactionRepository;
    private final TransactionsRepository transactionsRepository;

    @Scheduled(cron = "${spring.scheduled.archive-cron}")
    public void archiveCompletedTransactions() {
        List<Transaction> completedTransactions = transactionRepository.findByStatus(TransactionStatus.RETURNED);
        for (Transaction transaction : completedTransactions) {
            Transactions archivedTransaction = new Transactions();
            archivedTransaction.setStartDate(transaction.getStartDate());
            archivedTransaction.setEndDate(transaction.getEndDate());
            archivedTransaction.setStatus(transaction.getStatus());
            archivedTransaction.setUserId(transaction.getUser().getId());
            archivedTransaction.setBookId(transaction.getBook().getId());
            transactionsRepository.save(archivedTransaction);
            transactionRepository.delete(transaction);
        }
    }
}

