package com.example.library.service;


import com.example.library.models.primary.dto.JwtRequest;
import com.example.library.models.primary.dto.JwtResponse;

public interface AuthService {
    JwtResponse login(JwtRequest loginRequest);
}
