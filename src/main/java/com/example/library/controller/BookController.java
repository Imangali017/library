package com.example.library.controller;

import com.example.library.mappers.BookMapper;
import com.example.library.models.primary.Book;
import com.example.library.models.primary.dto.BookDto;
import com.example.library.models.primary.dto.BookSearchDto;
import com.example.library.service.BookService;
import com.example.library.service.CacheBookScheduler;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;
    private final BookMapper bookMapper;
    private final CacheManager cacheManager;
    private final CacheBookScheduler cacheBookScheduler;

    @GetMapping("/{bookId}")
    public BookDto getBook(@PathVariable Long bookId) {
        Book book = bookService.getBookById(bookId);
        return bookMapper.toDto(book);
    }

    @PostMapping
    public BookDto createBook(@RequestBody BookDto bookDto) {
        Book book = bookMapper.toEntity(bookDto);
        Book createdBook = bookService.createBook(book);
        return bookMapper.toDto(createdBook);
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
        bookService.deleteBook(bookId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/top10")
    public List<BookDto> getTop10BooksByViews() {
        Cache top10BooksCache = cacheManager.getCache("top10BooksCache");
        if (top10BooksCache == null) {
            return getTop10BooksByViewsAndMapToDto();
        }
        Cache.ValueWrapper top10 = top10BooksCache.get("top10");
        if (top10 == null) {
            return getTop10BooksByViewsAndMapToDto();
        }
        Page<Book> top10BooksPage = (Page<Book>) top10.get();
        List<Book> top10BooksList = Objects.requireNonNull(top10BooksPage).getContent();
        return top10BooksList.stream().map(bookMapper::toDto).collect(Collectors.toList());
    }

    public List<BookDto> getTop10BooksByViewsAndMapToDto() {
        List<Book> top10Books = cacheBookScheduler.getTop10BooksByViewsFromService();
        return top10Books.stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping
    public List<BookDto> getAllBooks() {
        return bookMapper.toDto(bookService.getAllBooks());
    }

    @GetMapping("/search")
    public List<BookDto> searchBooks(@RequestBody BookSearchDto searchDto) {
        List<Book> filteredBooks = bookService.searchBooksByCriteria(searchDto);
        return filteredBooks.stream().map(bookMapper::toDto).collect(Collectors.toList());
    }


}
