package com.example.library.mappers;

import com.example.library.models.primary.User;
import com.example.library.models.primary.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDto,User> {
    UserDto toDto(User entity);
    User toEntity(UserDto dto);
}
