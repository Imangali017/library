package com.example.library.mappers;

import com.example.library.models.primary.Book;
import com.example.library.models.primary.dto.BookDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper extends EntityMapper<BookDto, Book> {

    BookDto toDto(Book entity);
    Book toEntity(BookDto dto);
    List<Book> toEntity(List<BookDto> dtoList);
    List<BookDto> toDto(List<Book> entityList);
}
