
-- db/migration-archive/V5__create_transaction_table.sql

CREATE TABLE transactions (
                             id SERIAL PRIMARY KEY,
                             start_date TIMESTAMP NOT NULL,
                             end_date TIMESTAMP,
                             status VARCHAR(255) NOT NULL,
                             user_id BIGINT,
                             book_id BIGINT
);
