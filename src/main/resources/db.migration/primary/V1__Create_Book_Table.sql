-- db/migration/V1__create_books_table.sql

CREATE TABLE book (
                      id SERIAL PRIMARY KEY,
                      title VARCHAR(255) NOT NULL,
                      author VARCHAR(255) NOT NULL,
                      category VARCHAR(255),
                      year INT,
                      description TEXT,
                      views INT,
                      cover_url VARCHAR(255)
);
