CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255) NOT NULL,
                       username VARCHAR(255) NOT NULL,
                       password VARCHAR(255) NOT NULL,
                       email VARCHAR(255) NOT NULL,
                       password_confirmation VARCHAR(255),
                       role VARCHAR(255)
);

CREATE TABLE users_roles (
                             user_id BIGINT,
                             role VARCHAR(255)
);

ALTER TABLE users_roles
    ADD CONSTRAINT fk_user_role
        FOREIGN KEY (user_id)
            REFERENCES users(id);
