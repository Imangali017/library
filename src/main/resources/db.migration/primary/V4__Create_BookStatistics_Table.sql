CREATE TABLE book_statistics (
                                 id SERIAL PRIMARY KEY,
                                 book_id BIGINT NOT NULL,
                                 views INT DEFAULT 0,
                                 likes INT DEFAULT 0,
                                 FOREIGN KEY (book_id) REFERENCES book(id)
);
