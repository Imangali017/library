CREATE TABLE transaction (
                             id SERIAL PRIMARY KEY,
                             start_date TIMESTAMP NOT NULL,
                             end_date TIMESTAMP,
                             status VARCHAR(255) NOT NULL,
                             user_id BIGINT,
                             book_id BIGINT,
                             FOREIGN KEY (user_id) REFERENCES users(id),
                             FOREIGN KEY (book_id) REFERENCES book(id)
);
